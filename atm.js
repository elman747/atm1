let balans = 4000;
const newLine = "\n\r";

let atmMetin =
  "Emeliyyati secin:" +
  newLine +
  "1 - balans yoxlama" +
  newLine +
  "2 - pul cekme" +
  newLine +
  "3 - balans artirma" +
  newLine +
  "4 - karti qaytarma";

let operation = prompt(atmMetin);

switch (operation) {
  case "1":
    alert("Cari balansiniz:" + " " + balans + " " + "man");
    break;

  case "2":
    let teleb = Number(prompt("Almaq istediyiniz mebleqi daxil edin:"));
    let qaliqBalans = balans - teleb;
    if (balans > teleb) {
      alert(
        "Cari balansiniz:" +
          " " +
          balans +
          " " +
          "man" +
          newLine +
          "Almaq istediyiniz mebleq:" +
          " " +
          teleb +
          " " +
          "man"
      );

      confirm("Əminmisiniz?");

      alert(
        "Əməliyyat yerinə yetirildi:" +
          newLine +
          "Mexaric:" +
          " " +
          teleb +
          " " +
          "man" +
          newLine +
          "Qaliq balans:" +
          " " +
          qaliqBalans +
          " " +
          "man"
      );
    } else {
      alert("Balansınızda kifayət qədər vəsait yoxdur!!!");
    }
    break;

  case "3":
    let medaxil = Number(prompt("Artırmaq istediyiniz mebleği daxil edin:"));
    let cariBalans = balans + medaxil;
    alert(
      "Balansınız:" + " "+
        balans +
        " " +
        "man" +
        newLine +
        "Əlavə etdiyiniz məbləğ:" +
        " " +
        medaxil +
        " " +
        "man" +
        newLine +
        "Cari balans:" +
        " " +
        cariBalans +
        " " +
        "man"
    );
    break;

  case "4":
    confirm("Çıxmaq istədiyinizdən əminmisiniz?");
    alert("Əməliyyat uğurla yerinə yetirildi");
    break;

  default:
    alert("Seçdiyiniz əməliyyat siyahıda yoxdur!!!");
}
